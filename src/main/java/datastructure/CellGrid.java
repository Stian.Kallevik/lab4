package datastructure;

import javax.sql.RowSet;

import cellular.CellState;

public class CellGrid implements IGrid {

    //constructor
    private CellState[][] cellModes;


    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub

        cellModes = new CellState[rows][columns];  

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return cellModes.length;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cellModes[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        cellModes[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return cellModes[row][column]; 
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        IGrid copyObj = new CellGrid(numRows(),numColumns(), CellState.ALIVE); // making a new CellGrid

        for(int i = 0; i < numRows();i++) {
            for (int j = 0; j < numColumns();j++) { // nested loops to iterate through each index in the list
                copyObj.set(i, j, cellModes[i][j]); //copying each index from the original list to the new one.
            } 
        }
        return copyObj;
    }
    
}
