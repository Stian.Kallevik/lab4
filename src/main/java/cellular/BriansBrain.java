package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
    

	IGrid currentGeneration;
	int neighborCounter = 0;

	/**
	 * 
	 * Construct a BriansBrain Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		//System.out.println(currentGeneration.get(-1,0));
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		
		IGrid nextGeneration = currentGeneration.copy();
		// TODO

		for (int i = 0; i < numberOfRows(); i++) {
			for (int j = 0; j < numberOfColumns(); j++) {
				if (this.getNextCell(i,j) == CellState.ALIVE) {
					nextGeneration.set(i, j, CellState.ALIVE);
				} else if (this.getNextCell(i,j) == CellState.DYING) {
					nextGeneration.set(i, j, CellState.DYING);
 				}else {
					nextGeneration.set(i, j, CellState.DEAD);
				}
			}

		}
		
		currentGeneration = nextGeneration;
	}
    // Rules:
    // - En levende celle blir døende - OK
    // - En døende celle blir død - OK
    // - En død celle med akkurat 2 levende naboer blir levende
    // - En død celle forblir død ellers
	@Override
	public CellState getNextCell(int row, int col) {
		// TODO
		CellState checkCell = getCellState(row, col);
		int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);
		//int deadNeighbors = countNeighbors(row, col, CellState.DEAD);
		
		// System.out.println(currentGeneration);
		// System.out.println(checkCell);
		// System.out.println(aliveNeighbors);
		if (checkCell.equals(CellState.ALIVE)) {
			//System.out.println("Returns: " + CellState.DYING);
            return CellState.DYING;    
		} else if (checkCell.equals(CellState.DYING)) {
			//System.out.println("Returns: " + CellState.DEAD);
            return CellState.DEAD;
        } else {
			if (aliveNeighbors == 2) {
				//System.out.println("Returns: " + CellState.ALIVE);
				return CellState.ALIVE;
			} else {
				//System.out.println("Returns: " + CellState.DEAD);
				return CellState.DEAD;
			}
		}
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		// TODO - returns number of neighbors with the SAME CellState
		//int neighborCounter = 0;
		CellState centerStatus = getCellState(row, col);

		if (centerStatus == CellState.ALIVE){
			this.neighborCounter = -1; // it counts itself therefore starting on -1
		} else {
			this.neighborCounter = 0; // it does not count itself therefore starting on 0
		}
		

		for (int i = row-1; i < row + 2; i++) {
			for (int j = col-1; j < col + 2; j++) {
				try {
					if (currentGeneration.get(i,j).equals(state)) {
						this.neighborCounter++;	
					}
				} catch (ArrayIndexOutOfBoundsException exception) {
				}
			}
		}
		return this.neighborCounter;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
